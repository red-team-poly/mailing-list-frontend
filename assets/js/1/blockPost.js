let formaTag = document.querySelector('form#tag-settings');
let formaFilter = document.querySelector('form#filter-settings');
let formaUser = document.querySelector('form#users-settings');

document.querySelector("input[type='search']").addEventListener("keydown", function(e) {
    funcBlockPost(e);
});

if (formaTag) {
    formaTag.addEventListener("keydown", function(e) {
        funcBlockPost(e);
    });
}

if (formaFilter) {
    formaFilter.addEventListener("keydown", function(e) {
        funcBlockPost(e);
    });
}

if (formaUser) {
    formaUser.addEventListener("keydown", function(e) {
        funcBlockPost(e);
    });
}

function funcBlockPost(e) {
    if(e.keyCode === 13) {
        e.preventDefault();
    }
}