window.onresize = function() {
    let block = document.querySelector('.left-column-25').clientHeight;

    let inblockfilterup = document.querySelector('.filter-categories').clientHeight;
    let inblockfilterdown = document.querySelector('.filter-list');
    let newHieghtFilter = ((100 * inblockfilterup) / block) + 5;
    inblockfilterdown.style.top = newHieghtFilter.toString() + '%';

    if (document.documentElement.clientWidth >= 961) {
        document.body.style.overflow = 'hidden';
    } else {
        document.body.style.overflow = 'visible';
    }
};

/* ================================================= РАБОТА С ПОИСКОМ =============================================== */

let listFilter = document.querySelectorAll('.filters button');

document.getElementById('search-filters').onkeyup = function() {
    this.value = this.value.toLowerCase();
    let check = 0;

    if (radiosFilter[0].checked) {
        document.querySelectorAll('.filters button[data-type="public"]').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listFilter.length; i++)
            if (listFilter[i].getAttribute('style') === "display: none;")
                check++;
    } else if (radiosFilter[1].checked) {
        document.querySelectorAll('.filters button[data-type="local"], .filters button[data-type="publiclocal"]').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listFilter.length; i++)
            if (listFilter[i].getAttribute('style') === "display: none;")
                check++;
    } else if (radiosFilter[2].checked) {
        document.querySelectorAll('.filters button[data-type="publiclocal"]').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listFilter.length; i++)
            if (listFilter[i].getAttribute('style') === "display: none;")
                check++;
    } else {
        listFilter.forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listFilter.length; i++)
            if (listFilter[i].getAttribute('style') === "display: none;")
                check++;
    }

    if (check === listFilter.length)
        document.getElementById('nonresultfilter').style.display = 'block';
    else
        document.getElementById('nonresultfilter').style.display = 'none';
};
document.getElementById('search-filters').addEventListener('search', function () {
    document.getElementById('nonresultfilter').style.display = 'none';
    for (let i = 0; i < listFilter.length; i++)
        listFilter[i].style.display = '';

    if (radiosFilter[0].checked) {
        for (let i = 0; i < listFilter1.length; i++) {
            listFilter1[i].style.display = 'inline';
            if (listFilter1[i].getAttribute('data-type') !== "public")
                listFilter1[i].style.display = 'none';
        }
    }

    if (radiosFilter[1].checked) {
        for (let i = 0; i < listFilter1.length; i++) {
            listFilter1[i].style.display = 'inline';
            if (listFilter1[i].getAttribute('data-type') !== "local" &&
                listFilter1[i].getAttribute('data-type') !== "publiclocal")
                listFilter1[i].style.display = 'none';
        }
    }

    if (radiosFilter[2].checked) {
        for (let i = 0; i < listFilter1.length; i++) {
            listFilter1[i].style.display = 'inline';
            if (listFilter1[i].getAttribute('data-type') !== "publiclocal")
                listFilter1[i].style.display = 'none';
        }
    }
});

let selectTagFilter = document.getElementById('select-dropdown');
let listTagFilter = document.querySelectorAll('.list-tags-for-filter button');
let tagsArrayAll = [];
let tagsArray = [];

for (let i = 0; i < listTagFilter.length; i++) {
    let tag = listTagFilter[i].innerHTML.split("<");
    tagsArrayAll.push(tag[0].toLowerCase());
}

document.getElementById('search-tag-for-filter').onkeyup = function () {
    this.value = this.value.toLowerCase();

    let l = this.value.length;
    let check = 0;

    if (l === 0) {
        for (let i = 0; i < listTagFilter.length; i++) {
            listTagFilter[i].style.display = 'inline';
        }
        check = 0;
        document.getElementById('nonresulttagforfilter').style.display = 'none';
    }

    if (l > 0) {
        for (let i = 0; i < tagsArrayAll.length; i++) {
            listTagFilter[i].style.display = 'inline';
            let s = tagsArrayAll[i].split('').slice(0, l).join('');
            if (s !== this.value) {
                listTagFilter[i].style.display = 'none';
                check++;
            }

            if (check === tagsArrayAll.length)
                document.getElementById('nonresulttagforfilter').style.display = 'block';
            else
                document.getElementById('nonresulttagforfilter').style.display = 'none';
        }
    }
};
selectTagFilter.addEventListener('change', function() {
    document.getElementById('nonresulttagforfilter').style.display = 'none';
    document.getElementById('search-tag-for-filter').value = '';
    if (selectTagFilter.selectedIndex === 0) {
        tagsArrayAll = [];
        for (let i = 0; i < listTagFilter.length; i++) {
            listTagFilter[i].style.display = 'inline';
            let tagsAll = listTagFilter[i].innerHTML.split("<");
            tagsArrayAll.push(tagsAll[0].toLowerCase());
        }

        document.getElementById('search-tag-for-filter').onkeyup = function () {
            this.value = this.value.toLowerCase();

            let l = this.value.length;
            let check = 0;

            if (l === 0) {
                for (let i = 0; i < listTagFilter.length; i++) {
                    listTagFilter[i].style.display = 'inline';
                }
                check = 0;
                document.getElementById('nonresulttagforfilter').style.display = 'none';
            }

            if (l > 0) {
                for (let i = 0; i < tagsArrayAll.length; i++) {
                    listTagFilter[i].style.display = 'inline';
                    let s = tagsArrayAll[i].split('').slice(0, l).join('');
                    if (s !== this.value) {
                        listTagFilter[i].style.display = 'none';
                        check++;
                    }

                    if (check === tagsArrayAll.length)
                        document.getElementById('nonresulttagforfilter').style.display = 'block';
                    else
                        document.getElementById('nonresulttagforfilter').style.display = 'none';
                }
            }
        };
    }

    if (selectTagFilter.selectedIndex === 1) {
        tagsArray = [];
        for (let i = 0; i < listTagFilter.length; i++) {
            listTagFilter[i].style.display = 'inline';
            if (listTagFilter[i].getAttribute('data-type') !== "local" &&
                listTagFilter[i].getAttribute('data-type') !== "publiclocal")
                listTagFilter[i].style.display = 'none';
            else {
                let tagsLocal = listTagFilter[i].innerHTML.split("<");
                tagsArray.push(tagsLocal[0].toLowerCase());
            }
        }

        document.getElementById('search-tag-for-filter').onkeyup = function() {
            this.value = this.value.toLowerCase();

            let l = this.value.length;
            let check = 0;

            if (l === 0) {
                for (let i = 0; i < listTagFilter.length; i++) {
                    if (listTagFilter[i].getAttribute('data-type') === "local" ||
                        listTagFilter[i].getAttribute('data-type') === "publiclocal") {
                        listTagFilter[i].style.display = 'inline';
                    }
                }
                check = 0;
                document.getElementById('nonresulttagforfilter').style.display = 'none';
            }

            if (l > 0) {
                for (let i = 0; i < tagsArrayAll.length; i++) {
                    if (listTagFilter[i].getAttribute('data-type') === "local" ||
                        listTagFilter[i].getAttribute('data-type') === "publiclocal") {
                        listTagFilter[i].style.display = 'inline';
                        let s = tagsArrayAll[i].split('').slice(0, l).join('');
                        if (s !== this.value) {
                            listTagFilter[i].style.display = 'none';
                            check++;
                        }

                        if (check === tagsArray.length)
                            document.getElementById('nonresulttagforfilter').style.display = 'block';
                        else
                            document.getElementById('nonresulttagforfilter').style.display = 'none';
                    }
                }
            }
        };
    }

    if (selectTagFilter.selectedIndex === 2) {
        tagsArray = [];
        for (let i = 0; i < listTagFilter.length; i++) {
            listTagFilter[i].style.display = 'inline';
            if (listTagFilter[i].getAttribute('data-type') !== "public" &&
                listTagFilter[i].getAttribute('data-type') !== "publiclocal")
                listTagFilter[i].style.display = 'none';
            else {
                let tagsLocal = listTagFilter[i].innerHTML.split("<");
                tagsArray.push(tagsLocal[0].toLowerCase());
            }
        }

        document.getElementById('search-tag-for-filter').onkeyup = function() {
            this.value = this.value.toLowerCase();

            let l = this.value.length;
            let check = 0;

            if (l === 0) {
                for (let i = 0; i < listTagFilter.length; i++) {
                    if (listTagFilter[i].getAttribute('data-type') === "public" ||
                        listTagFilter[i].getAttribute('data-type') === "publiclocal") {
                        listTagFilter[i].style.display = 'inline';
                    }
                }
                check = 0;
                document.getElementById('nonresulttagforfilter').style.display = 'none';
            }

            if (l > 0) {
                for (let i = 0; i < tagsArrayAll.length; i++) {
                    if (listTagFilter[i].getAttribute('data-type') === "public" ||
                        listTagFilter[i].getAttribute('data-type') === "publiclocal") {
                        listTagFilter[i].style.display = 'inline';
                        let s = tagsArrayAll[i].split('').slice(0, l).join('');
                        if (s !== this.value) {
                            listTagFilter[i].style.display = 'none';
                            check++;
                        }

                        if (check === tagsArray.length)
                            document.getElementById('nonresulttagforfilter').style.display = 'block';
                        else
                            document.getElementById('nonresulttagforfilter').style.display = 'none';
                    }
                }
            }
        };
    }
});
document.getElementById('search-tag-for-filter').addEventListener('search', function () {
    document.getElementById('nonresulttagforfilter').style.display = 'none';
    if (selectTagFilter.selectedIndex === 0) {
        for (let i = 0; i < listTagFilter.length; i++)
            listTagFilter[i].style.display = 'inline';
    }

    if (selectTagFilter.selectedIndex === 1) {
        for (let i = 0; i < listTagFilter.length; i++) {
            listTagFilter[i].style.display = 'inline';
            if (listTagFilter[i].getAttribute('data-type') !== "local" &&
                listTagFilter[i].getAttribute('data-type') !== "publiclocal")
                listTagFilter[i].style.display = 'none';
        }
    }

    if (selectTagFilter.selectedIndex === 2) {
        for (let i = 0; i < listTagFilter.length; i++) {
            listTagFilter[i].style.display = 'inline';
            if (listTagFilter[i].getAttribute('data-type') !== "public" &&
                listTagFilter[i].getAttribute('data-type') !== "publiclocal")
                listTagFilter[i].style.display = 'none';
        }
    }
});

/* =============================================== РАБОТА С РАДИОБАТОН ============================================== */

let listFilter1 = document.querySelectorAll('.filters button');
let radiosFilter = document.querySelectorAll('.filter-categories input');

let filtersArrayAll = [];
let filtersArray = [];

for (let i = 0; i < listFilter.length; i++) {
    let filtersAll = listFilter[i].innerHTML.split("<");
    filtersArrayAll.push(filtersAll[0].toLowerCase());
}

let selectFilter = -1;
let checkedFilter = 0;

for (let i = 0; i < radiosFilter.length; i++) {
    radiosFilter[i].addEventListener('click', function() {
        document.getElementById('search-filters').value = '';
        let thisclick = 0;
        let check = 0;

        if (selectFilter === -1)
            selectFilter = i;

        if (i === selectFilter) {
            if (checkedFilter === 0) {
                checkedFilter = 1;
                selectFilter = i;
            } else {
                if (this.value)
                    thisclick = 1;
                this.checked = false;
                selectFilter = -1;
                checkedFilter = 0;
            }
        } else {
            selectFilter = i;
            checkedFilter = 1;
        }

        if (i === 0 && thisclick === 0) {
            filtersArray = [];
            for (let i = 0; i < listFilter1.length; i++) {
                listFilter1[i].style.display = '';
                if (listFilter1[i].getAttribute('data-type') !== "public")
                    listFilter1[i].style.display = 'none';
                else {
                    let tagsLocal = listFilter1[i].innerHTML.split("<");
                    filtersArray.push(tagsLocal[0].toLowerCase());
                }
            }
        } else if (i === 1 && thisclick === 0) {
            filtersArray = [];
            for (let i = 0; i < listFilter1.length; i++) {
                listFilter1[i].style.display = '';
                if (listFilter1[i].getAttribute('data-type') !== "local" &&
                    listFilter1[i].getAttribute('data-type') !== "publiclocal")
                    listFilter1[i].style.display = 'none';
                else {
                    let tagsLocal = listFilter1[i].innerHTML.split("<");
                    filtersArray.push(tagsLocal[0].toLowerCase());
                }
            }
        } else if (i === 2 && thisclick === 0) {
            filtersArray = [];
            for (let i = 0; i < listFilter1.length; i++) {
                listFilter1[i].style.display = '';
                if (listFilter1[i].getAttribute('data-type') !== "publiclocal")
                    listFilter1[i].style.display = 'none';
                else {
                    let tagsLocal = listFilter1[i].innerHTML.split("<");
                    filtersArray.push(tagsLocal[0].toLowerCase());
                }
            }
        } else if (thisclick === 1)
            for (let i = 0; i < listFilter1.length; i++)
                listFilter1[i].style.display = '';

        for (let i = 0; i < listFilter1.length; i++)
            if (listFilter1[i].getAttribute('style') === "display: none;")
                check++;

        if (check === listFilter1.length)
            document.getElementById('nonresultfilter').style.display = 'block';
        else
            document.getElementById('nonresultfilter').style.display = 'none';
    });
}

/* ========================================== РАБОТА С ТЭГАМИ И ОПЕРАТОРАМИ ========================================= */

let input = document.getElementById('input-cle').value;
let blockInput = document.getElementById('blockInputCLE');
let tags = document.querySelectorAll('.list-tags-for-filter button');
let operators = document.querySelectorAll('.list-operators-for-filter button');
let clear = document.querySelector('.clear-cle button');

let blockAlert = document.getElementById('blockAlert');

let ckeckBracket = 0;

function alertVis(e) {
    if (blockAlert.style.display === 'none') {
        blockAlert.style.display = 'block';
        blockAlert.style.left = (e.clientX - 55) + 'px';
        blockAlert.style.top = (e.clientY - 60) + 'px';
        setTimeout(function() { blockAlert.style.display = 'none'; }, 1000);
    } else {
        blockAlert.style.display = 'none';
    }
}

for (let i = 0; i < tags.length; i++) {
    tags[i].addEventListener('click', function (e) {
        let split = input.split("  ");
        let count = split.length;
        let last = split[count - 1];
        let inputreplace = "";

        if (last === "AND" || last === "OR" || last === "NOT" || last === "(") {
            if (last === ")") {
                alertVis(e);
            } else
                input += "  " + tags[i].innerHTML;
        } else {
            if (last === ")") {
                alertVis(e);
            } else {
                for (let i = 1; i < split.length; i++)
                    if (inputreplace === "")
                        inputreplace += split[i - 1];
                    else
                        inputreplace += "  " + split[i - 1];

                input = inputreplace;
                if (input.length === 0)
                    input += tags[i].innerHTML;
                else
                    input += "  " + tags[i].innerHTML;
            }
        }
        document.getElementById('input-cle').value = input.toUpperCase();
        blockInput.innerText = input.toUpperCase();
    });
}

for (let i = 0; i < operators.length; i++) {
    operators[i].addEventListener('click', function (e) {
        let split = input.split("  ");
        let count = split.length;
        let last = split[count - 1];
        let inputreplace = "";

        if (last === "AND" || last === "OR" || last === "NOT" || last === "(" || last === ")") {
            if (operators[i].innerHTML.length === 1) {
                if (operators[i].innerHTML === "(" && last !== ")") {
                    if (operators[i].innerHTML === ")") {
                        alertVis(e);
                    } else
                        input += "  " + operators[i].innerHTML;

                    ckeckBracket += 1;
                }
                if (operators[i].innerHTML === ")" && last === ")") {
                    if (operators[i].innerHTML === ")")
                        if (ckeckBracket > 0) {
                            input += "  " + operators[i].innerHTML;
                            ckeckBracket -= 1;
                        } else
                            alertVis(e);
                }
                if (operators[i].innerHTML === "(" && last === ")") {
                    alertVis(e);
                }
            } else {
                if (last === "(" || last === ")") {
                    if (last === ")")
                        if (operators[i].innerHTML === "NOT") {
                            alertVis(e);
                        } else
                            input += "  " + operators[i].innerHTML;
                    if (last === "(") {
                        if (operators[i].innerHTML === "NOT")
                            input += "  " + operators[i].innerHTML;
                        else
                            alertVis(e);
                    }
                } else {
                    if (operators[i].innerHTML === "NOT") {
                        if (last === "AND" || last === "OR" || last === "(") {
                            input += "  " + operators[i].innerHTML;
                        } else
                            alertVis(e);
                    }

                    if (operators[i].innerHTML === "AND" || operators[i].innerHTML === "OR") {
                        if (last !== "NOT") {
                            for (let i = 1; i < split.length; i++)
                                if (inputreplace === "")
                                    inputreplace += split[i - 1];
                                else
                                    inputreplace += "  " + split[i - 1];

                            input = inputreplace;
                            input += "  " + operators[i].innerHTML;
                        } else
                            alertVis(e);
                    }
                }
            }
            document.getElementById('input-cle').value = input.toUpperCase();
            blockInput.innerText = input.toUpperCase();
        } else {
            if (input.length > 0) {
                if (operators[i].innerHTML === "AND" || operators[i].innerHTML === "OR")
                    input += "  " + operators[i].innerHTML;

                if (operators[i].innerHTML === "NOT") {
                    if (last === "AND" || last === "OR" || last === "(")
                        input += "  " + operators[i].innerHTML;
                }

                if (ckeckBracket > 0) {
                    if (operators[i].innerHTML === ")") {
                        input += "  " + operators[i].innerHTML;
                        ckeckBracket -= 1;
                    }
                }
            } else {
                if (operators[i].innerHTML === "(" || operators[i].innerHTML === "NOT") {
                    if (operators[i].innerHTML === "(") {
                        input += operators[i].innerHTML;
                        ckeckBracket += 1;
                    }

                    if (operators[i].innerHTML === "NOT") {
                        input += operators[i].innerHTML;
                    }
                }
            }
            document.getElementById('input-cle').value = input.toUpperCase();
            blockInput.innerText = input.toUpperCase();
        }

        if (blockInput.innerText.length === 0)
            blockInput.innerText = "Логическое выражение";
    });
}

clear.addEventListener('click', function() {
    let split = input.split("  ");
    let count = split.length;
    let inputclear = "";
    let delElem;

    for (let i = 1; i < split.length; i++)
        if (inputclear === "")
            inputclear += split[i - 1];
        else
            inputclear += "  " + split[i - 1];

    delElem = split[count - 1];

    if (delElem === "(")
        ckeckBracket -= 1;
    if (delElem === ")")
        ckeckBracket += 1;

    input = inputclear;
    document.getElementById('input-cle').value = input.toUpperCase();
    blockInput.innerText = input.toUpperCase();

    if (blockInput.innerText.length === 0)
        blockInput.innerText = "Логическое выражение";
});


let form = document.getElementById('filter-settings');
let inputNameFilter = document.getElementById('input-name-filter');
form.noValidate = true;
let preview, create = "";

window.onload = function() {
    function strPad() {
        if (this.value === "Предпросмотр") {
            preview = this.value;
            create = "";
        } else if (this.value === "Создать фильтр") {
            preview = "";
            create = this.value;
        }
    }

    let bt = document.getElementsByClassName("but");
    for (let i = 0; i < bt.length; i++) {
        bt[i].onclick = strPad;
    }
}

form.addEventListener('submit', function (e) {
    let split = input.split("  ");
    let count = split.length;
    let last = split[count - 1];
    let badreq = 0;

    if (preview === "Предпросмотр" || create === "Создать фильтр") {
        if (inputNameFilter.value.length === 0) {
            alert('Введите название фильтра.');
            e.preventDefault();
            inputNameFilter.focus();
            badreq = 1;
        } else if (input.length === 0) {
            alert('Поле с формулой пустое!');
            e.preventDefault();
            badreq = 1;
        } else if (ckeckBracket !== 0 || last === "AND" || last === "OR" || last === "NOT" || last === "(") {
            alert('Некорректная формула фильтра.');
            e.preventDefault();
            badreq = 1;
        }
    }

    if (badreq === 0) {
        let newstr = input;
        newstr = newstr.replace(/ {2}AND {2}/gi, ' & ');
        newstr = newstr.replace(/ {2}OR {2}/gi, ' | ');
        newstr = newstr.replace(/ {2}NOT {2}/gi, '!');
        newstr = newstr.replace(/NOT {2}/gi, '!');

        newstr = newstr.replace(/\( {2}/gi, '(');
        newstr = newstr.replace(/ {2}\)/gi, ')');

        // document.getElementById('input-cle').value = newstr.toUpperCase();
        document.getElementById('input-cle').value = newstr;

        // alert('Отправляемая формула: ' + newstr);
        // e.preventDefault();
    }
}, false);

/* ================================================ КОНТЕКСТНОЕ МЕНЮ ================================================ */

let contextMenuFilter = document.querySelectorAll(".context-menu-filter");
let contextMenuOpenFilter = document.querySelector(".context-menu-filter-open");
let bakcgroundWindow = document.querySelector(".back-ground-show");

for (let i = 0; i < contextMenuFilter.length; i++) {
    contextMenuFilter[i].addEventListener('contextmenu', function(e) {
        e.preventDefault();
        contextMenuOpenFilter.style.left = e.clientX + 'px';
        contextMenuOpenFilter.style.top = e.clientY + 'px';
        contextMenuOpenFilter.style.display = 'block';
    });
}

window.addEventListener('click', function() {
    contextMenuOpenFilter.style.display = 'none';
});


let delFilterWindowOpen = document.querySelector(".deleted-filter");

document.querySelector(".del-filter").addEventListener('click', function() {
    contextMenuOpenFilter.style.display = 'none';
    delFilterWindowOpen.style.display = 'block';
    bakcgroundWindow.style.display = 'block';

    if (document.documentElement.clientWidth <= 960) {
        document.body.style.overflow = 'hidden';
        document.body.style.top = `-${window.scrollY}px`;
    }
});

window.addEventListener('scroll', function() {
    contextMenuOpenFilter.style.display = 'none';
    if (document.documentElement.clientWidth <= 960) {
        document.body.style.overflow = 'visible';
    }
});

document.getElementById('form-filters').onscroll = function() {
    contextMenuOpenFilter.style.display = 'none';
    if (document.documentElement.clientWidth <= 960) {
        document.body.style.overflow = 'visible';
    }
};

/* =============================================== ОКНО ПОДТВЕРЖДЕНИЯ =============================================== */

document.getElementById('close-del-filter').onclick = function() {
    delFilterWindowOpen.style.display = 'none';
    bakcgroundWindow.style.display = 'none';
    if (document.documentElement.clientWidth <= 960) {
        document.body.style.overflow = 'visible';
    }
};