window.onresize = function() {
    let block = document.querySelector('.left-column-25').clientHeight;

    let inblocktagup = document.querySelector('.tag-categories').clientHeight;
    let inblocktagdown = document.querySelector('.tag-list');
    let newHieghtTag = ((100 * inblocktagup) / block) + 5;
    inblocktagdown.style.top = newHieghtTag.toString() + '%';

    if (document.documentElement.clientWidth >= 961) {
        document.body.style.overflow = 'hidden';
    } else {
        document.body.style.overflow = 'visible';
    }
};

/* ================================================= РАБОТА С ПОИСКОМ =============================================== */

let listTag = document.querySelectorAll('.list-tag li');

document.getElementById('search-tag').onkeyup = function () {
    this.value = this.value.toLowerCase();
    let check = 0;

    if (radiosTag[1].checked) {
        document.querySelectorAll('.list-tag li[data-type="public"]').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listTag.length; i++)
            if (listTag[i].getAttribute('style') === "display: none;")
                check++;
    } else if (radiosTag[2].checked) {
        document.querySelectorAll('.list-tag li[data-type="local"], .list-tag li[data-type="publiclocal"]').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listTag.length; i++)
            if (listTag[i].getAttribute('style') === "display: none;")
                check++;
    } else if (radiosTag[3].checked) {
        document.querySelectorAll('.list-tag li[data-type="publiclocal"]').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listTag.length; i++)
            if (listTag[i].getAttribute('style') === "display: none;")
                check++;
    }
    else {
        listTag.forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listTag.length; i++)
            if (listTag[i].getAttribute('style') === "display: none;")
                check++;
    }

    if (check === listTag.length)
        document.getElementById('nonresulttag').style.display = 'block';
    else
        document.getElementById('nonresulttag').style.display = 'none';
};
document.getElementById('search-tag').addEventListener('search', function () {
    document.getElementById('nonresulttag').style.display = 'none';
    for (let i = 0; i < listTag.length; i++)
        listTag[i].style.display = 'inline';

    if (radiosTag[1].checked) {
        for (let i = 0; i < listTag1.length; i++) {
            listTag1[i].style.display = 'inline';
            if (listTag1[i].getAttribute('data-type') !== "public")
                listTag1[i].style.display = 'none';
        }
    }

    if (radiosTag[2].checked) {
        for (let i = 0; i < listTag1.length; i++) {
            listTag1[i].style.display = 'inline';
            if (listTag1[i].getAttribute('data-type') !== "local" &&
                listTag1[i].getAttribute('data-type') !== "publiclocal")
                listTag1[i].style.display = 'none';
        }
    }

    if (radiosTag[3].checked) {
        for (let i = 0; i < listTag1.length; i++) {
            listTag1[i].style.display = 'inline';
            if (listTag1[i].getAttribute('data-type') !== "publiclocal")
                listTag1[i].style.display = 'none';
        }
    }
});

document.getElementById('search-student').onkeyup = function() {
    this.value = this.value.toLowerCase();
    let check = 0;

    if (/^(1|[1-5]\d*)$/.test(this.value) && this.value.length === 1) {
        document.querySelectorAll('#table-student tr td:nth-child(4)').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.parentElement.style.display = '';
            else
                e.parentElement.style.display = 'none';
        })
    } else {
        document.querySelectorAll('#table-student tr').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })
    }

    for (let i = 0; i < document.querySelectorAll('#table-student tr').length; i++)
        if (document.querySelectorAll('#table-student tr')[i].getAttribute('style') === "display: none;")
            check++;

    if (check === document.querySelectorAll('#table-student tr').length)
        document.getElementById('nonresultstudent').style.display = 'block';
    else
        document.getElementById('nonresultstudent').style.display = 'none';
};
document.getElementById('search-student').addEventListener('search', function () {
    document.getElementById('nonresultstudent').style.display = 'none';
    for (let i = 0; i < document.querySelectorAll('#table-student tr').length; i++)
        document.querySelectorAll('#table-student tr')[i].style.display = '';
});

/* =============================================== РАБОТА С РАДИОБАТОН ============================================== */

let listTag1 = document.querySelectorAll('.list-tag li');
let radiosTag = document.querySelectorAll('.tag-categories input');
let selectTag = -1;
let checkedTag = 0;
let tagsArray = [];

for (let i = 0; i < radiosTag.length; i++) {
    radiosTag[i].addEventListener('click', function() {
        document.getElementById('search-tag').value = '';
        let thisclick = 0;
        let check = 0;
        if (selectTag === -1)
            selectTag = i;

        if (i === selectTag) {
            if (checkedTag === 0) {
                checkedTag = 1;
                selectTag = i;
            } else {
                if (this.value)
                    thisclick = 1;
                this.checked = false;
                selectTag = -1;
                checkedTag = 0;
            }
        } else {
            selectTag = i;
            checkedTag = 1;
        }

        if (i === 1 && thisclick === 0) {
            tagsArray = [];
            for (let i = 0; i < listTag1.length; i++) {
                listTag1[i].style.display = 'inline';
                if (listTag1[i].getAttribute('data-type') !== "public")
                    listTag1[i].style.display = 'none';
                else {
                    let tagsLocal = listTag1[i].innerHTML.split("<");
                    tagsArray.push(tagsLocal[0].toLowerCase());
                }
            }
        } else if (i === 2 && thisclick === 0) {
            tagsArray = [];
            for (let i = 0; i < listTag1.length; i++) {
                listTag1[i].style.display = 'inline';
                if (listTag1[i].getAttribute('data-type') !== "local" &&
                    listTag1[i].getAttribute('data-type') !== "publiclocal")
                    listTag1[i].style.display = 'none';
                else {
                    let tagsLocal = listTag1[i].innerHTML.split("<");
                    tagsArray.push(tagsLocal[0].toLowerCase());
                }
            }
        } else if (i === 3 && thisclick === 0) {
            tagsArray = [];
            for (let i = 0; i < listTag1.length; i++) {
                listTag1[i].style.display = 'inline';
                if (listTag1[i].getAttribute('data-type') !== "publiclocal")
                    listTag1[i].style.display = 'none';
                else {
                    let tagsLocal = listTag1[i].innerHTML.split("<");
                    tagsArray.push(tagsLocal[0].toLowerCase());
                }
            }
        } else if (thisclick === 1)
            for (let i = 0; i < listTag1.length; i++)
                listTag1[i].style.display = 'inline';

        for (let i = 0; i < listTag1.length; i++)
            if (listTag1[i].getAttribute('style') === "display: none;")
                check++;

        if (check === listTag1.length)
            document.getElementById('nonresulttag').style.display = 'block';
        else
            document.getElementById('nonresulttag').style.display = 'none';
    });
}

/* ============================================= УПРАВЛЕНИЕ СТУДЕНТАМИ ============================================== */

let addStud = document.querySelector(".panel-management button.add-student");
let delStud = document.querySelector(".panel-management button.del-student");

addStud.addEventListener('click', function() {
    let x = document.querySelectorAll("table#table-student tr td:nth-child(1) input");
    for (let i = 0; i < x.length; i++) {
        console.log(document.querySelectorAll("table#table-student tr td:nth-child(1) input").length)
        if (x[i].checked) {
            document.querySelector("table#table-student-in-tag tbody").appendChild(x[i].parentElement.parentElement);
        }
    }
});

delStud.addEventListener('click', function() {
    let x = document.querySelectorAll("table#table-student-in-tag tr td:nth-child(1) input");
    for (let i = 0; i < x.length; i++) {
        if (x[i].checked) {
            document.querySelector("table#table-student tbody").appendChild(x[i].parentElement.parentElement);
        }
    }
});

/* ================================================ КОНТЕКСТНОЕ МЕНЮ ================================================ */

let contextMenuTag = document.querySelectorAll(".context-menu-tag");
let contextMenuOpenTag = document.querySelector(".context-menu-tag-open");
let bakcgroundWindow = document.querySelector(".back-ground-show");

for (let i = 0; i < contextMenuTag.length; i++) {
    contextMenuTag[i].addEventListener('contextmenu', function(e) {
        e.preventDefault();
        contextMenuOpenTag.style.left = e.clientX + 'px';
        contextMenuOpenTag.style.top = e.clientY + 'px';
        contextMenuOpenTag.style.display = 'block';
    });
}

window.addEventListener('click', function() {
    contextMenuOpenTag.style.display = 'none';
});

window.addEventListener('scroll', function() {
    contextMenuOpenTag.style.display = 'none';
    if (document.documentElement.clientWidth <= 960) {
        document.body.style.overflow = 'visible';
    }
});

document.getElementById('form-tags').onscroll = function() {
    contextMenuOpenTag.style.display = 'none';
    if (document.documentElement.clientWidth <= 960) {
        document.body.style.overflow = 'visible';
    }
};

/* ================================================= ОКНО АТРИБУТОВ ================================================= */

let atributWindow = document.querySelectorAll(".atributes-show");
let atributOpen = document.querySelector(".right-column-atribut");

for (let i = 0; i < atributWindow.length; i++) {
    atributWindow[i].addEventListener('click', function(e) {
        e.preventDefault();
        atributOpen.style.display = 'block';
        bakcgroundWindow.style.display = 'block';

        if (document.documentElement.clientWidth <= 960) {
            document.body.style.overflow = 'hidden';
            document.body.style.top = `-${window.scrollY}px`;
        }
    });
}
document.getElementById('close-atribut').onclick = function() {
    atributOpen.style.display = 'none';
    bakcgroundWindow.style.display = 'none';
    if (document.documentElement.clientWidth <= 960) {
        document.body.style.overflow = 'visible';
    }
};

/* =============================================== ОКНО ПОДТВЕРЖДЕНИЯ =============================================== */

let delTagWindow = document.querySelectorAll(".del-tag");
let delTagWindowOpen = document.querySelector(".deleted-tag");

for (let i = 0; i < delTagWindow.length; i++) {
    delTagWindow[i].addEventListener('click', function(e) {
        e.preventDefault();
        delTagWindowOpen.style.display = 'block';
        bakcgroundWindow.style.display = 'block';

        if (document.documentElement.clientWidth <= 960) {
            document.body.style.overflow = 'hidden';
            document.body.style.top = `-${window.scrollY}px`;
        }
    });
}

document.getElementById('close-del-tag').onclick = function() {
    delTagWindowOpen.style.display = 'none';
    bakcgroundWindow.style.display = 'none';
    if (document.documentElement.clientWidth <= 960) {
        document.body.style.overflow = 'visible';
    }
};

document.addEventListener('DOMContentLoaded', function() {
    let table = document.querySelector('.tbl-content table');
    let headersStudent = document.querySelectorAll('.block-students .tbl-header th:not(:nth-child(1))');
    const tableBody = table.querySelector('tbody');
    const rows = tableBody.querySelectorAll('tr');

    // Направление сортировки
    const directions = Array.from(headersStudent).map(function() {
        return '';
    });

    const sortColumn = function(index) {
        const direction = directions[index] || 'asc';

        // Фактор по направлению
        const multiplier = (direction === 'asc') ? 1 : -1;

        const newRows = Array.from(rows);

        newRows.sort(function (rowA, rowB) {
            const cellA = rowA.querySelectorAll('td')[index + 1].innerHTML;
            const cellB = rowB.querySelectorAll('td')[index + 1].innerHTML;

            switch (true) {
                case cellA > cellB:
                    return 1 * multiplier;
                case cellA < cellB:
                    return -1 * multiplier;
                case cellA === cellB:
                    return 0;
            }
        });

        // Удалить старые строки
        [].forEach.call(rows, function (row) {
            tableBody.removeChild(row);
        });

        // Поменять направление
        directions[index] = direction === 'asc' ? 'desc' : 'asc';

        // Добавить новую строку
        newRows.forEach(function (newRow) {
            tableBody.appendChild(newRow);
        });

        for (let i = 0; i < headersStudent.length; i++) {
            headersStudent[i].className = "";
            if (direction === 'asc')
                headersStudent[index].className = "sort-down";
            else
                headersStudent[index].className = "sort-up";
        }
    };

    [].forEach.call(headersStudent, function(e, index) {
        e.addEventListener('click', function() {
            sortColumn(index);
        });
    });
});
